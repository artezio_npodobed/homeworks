﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b, c;
            double?[] roots;

            Console.WriteLine("Введите коэффициенты квадратного уравнения: ");

            a = Input("a");
            b = Input("b");
            c = Input("c");

            if (a != 0)
            {
                roots = Solve(a, b, c);
                OutputSolving(roots, a, b, c);
            }
            else
            {
                Console.WriteLine("Это линейное уравнение");
            }
            Console.Read();
        }

        private static double Input(string k)
        {
            double num;
            bool check;
            do
            {
                Console.Write("Коэффициент {0} = ", k);
                check = !double.TryParse(Console.ReadLine(), out num);
                if (check)
                {
                    Console.WriteLine("Ошибка ввода. Попробуйте еще раз:");
                }
            }
            while (check);
            return num;
        }

        private static double?[] Solve(double a, double b, double c)
        {
            double D = Math.Pow(b, 2) - 4 * a * c;
            double?[] solving;

            if (D < 0)
            {
                solving = new double?[] { null };
            }
            else if (D == 0)
            {
                solving = new double?[] { -b / 2 * a };
            }
            else
            {
                solving = new double?[] { (-b + Math.Sqrt(D)) / (2 * a), (-b - Math.Sqrt(D)) / (2 * a) };
            }
            return solving;
        }

        private static void OutputSolving(double?[] roots, double a, double b, double c)
        {
            Console.Clear();
            Console.WriteLine("Уравнение: {0: #;-#;0}*X^2 {1:+ #;- #;+ 0}*X {2:+ #;- #;+ 0} = 0\n", a, b, c);
            if (roots[0] == null)
            {
                Console.WriteLine("Нет корней на множестве действительных чисел");
            }
            else
            {
                Console.WriteLine("Решение: ");
                for (int i = 0; i < roots.Length; i++)
                {
                    Console.WriteLine("x{0} = {1} ", i + 1, roots[i]);
                }
            }
        }
    }
}

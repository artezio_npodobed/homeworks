function BillComponent() {
    var toppingsElement = document.getElementById('toppingsInBill');
    var stuffingsElement = document.getElementById('stuffingsInBill');
    var sizeNameElement = document.getElementById('sizeNameInBill');
    var sizePriceElement = document.getElementById('sizePriceInBill');
    var hamburgerPriceElement = document.getElementById('hamburgerPriceInBill');
    var hamburgersCountElement = document.getElementById('countInBill');
    var totalPriceElement = document.getElementById('totalPriceInBill');

    function toFormBill(hamburger, count) {
        var price = hamburger.calculatePrice();
        addSupplements(toppingsElement, hamburger.getToppings(), 'Toppings');
        addSupplements(stuffingsElement, hamburger.getStuffings(), 'Stuffings');

        sizeNameElement.textContent = hamburger.size.name;
        sizePriceElement.textContent = '$' + hamburger.size.price.toFixed(2);
        hamburgerPriceElement.textContent = '$' + price.toFixed(2);
        hamburgersCountElement.textContent = count;
        totalPriceElement.textContent = '$' + (count * price).toFixed(2);
    }

    function addSupplements(table, list, listName) {
        var hidden = list.length === 0;
        table.classList.toggle('hidden', hidden);
        if (hidden) {
            return;
        }

        while (table.rows.length > 0) {
            table.deleteRow(0);
        }
        addHeader(table, listName);
        list.forEach(function (item) {
            addRow(table, item.name, '$' + item.price.toFixed(2));
        });
    }

    function addHeader(table, headerName) {
        var tr = table.insertRow();
        var header = tr.insertCell();
        header.setAttribute('colspan', '2');
        header.classList.add('bill-section-header');
        header.textContent = headerName;
    }

    function addRow(table, name, price) {
        var row = table.insertRow();
        row.insertCell().textContent = name;
        row.insertCell().textContent = price;
    }

    this.toFormBill = toFormBill;
}
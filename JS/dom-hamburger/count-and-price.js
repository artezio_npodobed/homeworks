CountAndPriceComponent.MAX_COUNT = 50;
CountAndPriceComponent.MIN_COUNT = 1;

function CountAndPriceComponent() {
    var count = 1;
    var price = 0;
    var countInputElement = document.getElementById('count');
    var totalPriceElement = document.getElementById('totalPrice');

    countInputElement.addEventListener('input', onCountInputEvent);
    countInputElement.addEventListener('blur', onCountBlurEvent);

    function onCountBlurEvent(event) {
        var newCount = +event.target.value;
        if (newCount !== count) {
            count = newCount;
            recalculateTotalPrice();
        }
    }

    function onCountInputEvent(event) {
        var newCount = +event.target.value;
        if (newCount > CountAndPriceComponent.MAX_COUNT) {
            event.target.value = CountAndPriceComponent.MAX_COUNT;
        } else if (newCount < CountAndPriceComponent.MIN_COUNT) {
            event.target.value = CountAndPriceComponent.MIN_COUNT;
        }
    }

    function recalculateTotalPrice() {
        var totalPrice = (count * price).toFixed(2);
        totalPriceElement.textContent = '$' + totalPrice;
    }

    /**
     * Получение количества заказываемых гамбургеров
     * @returns {number} 
     */
    this.getCount = function () {
        return count;
    };

    /**
     * Получение общей цены за заказ (с учетом количества)
     * @return {string} Форматированная строка суммы с 2 знаками после запятой
     */
    this.getTotalPrice = function () {
        return totalPriceElement.textContent;
    };

    /**
     * Установка новой цены за 1 гамбургер.
     * @param {number} newPrice 
     */
    this.setPrice = function (newPrice) {
        price = newPrice;
        recalculateTotalPrice();
    };
}
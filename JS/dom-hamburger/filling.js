function FillingComponent(componentId, itemsList) {
    var itemAddListener = null;
    var itemRemoveListener = null;
    var component = document.getElementById(componentId);
    var tbodyElement = component.querySelector('.table').tBodies[0];
    var buttonElement = component.querySelector('.dropdown-toggle');
    var listElement = component.querySelector('.dropdown-menu');

    itemsList.forEach(addItemToListElement);
    tbodyElement.addEventListener('click', onTableClickEvent);
    listElement.addEventListener('click', onListClickEvent);

    function onTableClickEvent(event) {
        var target = event.target;

        while (target !== this) {
            if (target.classList.contains('btn-danger')) {
                var id = target.dataset.id;
                var item = itemsList[id];
                var rowElement = target.closest('tr');

                if (itemRemoveListener) {
                    itemRemoveListener(item);
                }
                addItemToListElement(item, id);
                tbodyElement.removeChild(rowElement);
                updateNumbers();
                buttonElement.disabled = false;
                return;
            }
            target = target.parentElement;
        }
    }

    function onListClickEvent(event) {
        var target = event.target;
        var id = target.dataset.id;
        if (!id) {
            return;
        }
        
        var item = itemsList[id];
        if (itemAddListener) {
            itemAddListener(item);
        }
        addItemToTable(item, id);
        listElement.removeChild(target.parentElement);
        buttonElement.disabled = listElement.children.length === 0;
    }

    function addItemToListElement(item, id) {
        var li = document.createElement('li');
        var a = document.createElement('a');

        a.dataset.id = id;
        a.setAttribute('href', '#');
        a.textContent = item.name;
        li.appendChild(a);
        listElement.appendChild(li);
    }

    function addItemToTable(item, id) {
        var row = tbodyElement.insertRow();

        var number = row.insertCell();
        number.textContent = tbodyElement.rows.length;

        var name = row.insertCell();
        name.setAttribute('width', '50%');
        name.textContent = item.name;

        var price = row.insertCell();
        price.setAttribute('width', '50%');
        price.textContent = '$' + item.price.toFixed(2);

        var tdButton = row.insertCell();
        tdButton.setAttribute('width', 'auto');
        var deleteButton = tdButton.appendChild(createDeleteButton());
        deleteButton.dataset.id = id;
    }

    function createDeleteButton() {
        var button = document.createElement('button');
        button.classList.add('btn', 'btn-sm', 'btn-danger');

        var span = document.createElement('span');
        span.classList.add('glyphicon', 'glyphicon-remove');

        button.appendChild(span);
        return button;
    }

    function updateNumbers() {
        var rows = tbodyElement.rows;
        for (var i = 0; i < rows.length; i++) {
            rows[i].cells[0].textContent = i + 1;
        }
    }

    this.onItemAdd = function (listener) {
        itemAddListener = listener;
    };

    this.onItemRemove = function (listener) {
        itemRemoveListener = listener;
    };
}
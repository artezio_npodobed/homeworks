function HamburgerInfoComponent(hamburgerInfoBlockId, hamburger) {
    var hamburgerInfoBlockElements = document.getElementById(hamburgerInfoBlockId).querySelectorAll('dd');
    var size = hamburgerInfoBlockElements[0];
    var toppingsCount = hamburgerInfoBlockElements[1];
    var stuffingsCount = hamburgerInfoBlockElements[2];
    var calories = hamburgerInfoBlockElements[3];
    var price = hamburgerInfoBlockElements[4];

    size.textContent = hamburger.size.name;
    stuffingsCount.textContent = hamburger.getStuffings().length;
    toppingsCount.textContent = hamburger.getToppings().length;
    updateCaloriesAndPrice();

    function updateCaloriesAndPrice() {
        calories.textContent = hamburger.calculateCalories();
        price.textContent = '$' + hamburger.calculatePrice().toFixed(2);
    }

    this.updateStuffings = function () {
        stuffingsCount.textContent = hamburger.getStuffings().length;
        updateCaloriesAndPrice();
    };

    this.updateToppings = function () {
        toppingsCount.textContent = hamburger.getToppings().length;
        updateCaloriesAndPrice();
    };
}
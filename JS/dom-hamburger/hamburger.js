var Hamburger = (function () {
    function Hamburger(size) {
        this.size = size;
        this.stuffings = [];
        this.toppings = [];
    }
    Hamburger.SIZE_SMALL = new Size('Small', 2.5, 400, 5);
    Hamburger.SIZE_LARGE = new Size('Large', 4, 800, 10);
    Hamburger.STUFFING_CHEESE = new Supplement('Cheese', 1, 150);
    Hamburger.STUFFING_SALAD = new Supplement('Salad', 0.5, 5);
    Hamburger.STUFFING_POTATO = new Supplement('Potato', 0.75, 40);
    Hamburger.TOPPING_MAYO = new Supplement('Mayo', 0.5, 300);
    Hamburger.TOPPING_SPICE = new Supplement('Spice', 0.25, 2);

    Hamburger.allToppings = [Hamburger.TOPPING_MAYO, Hamburger.TOPPING_SPICE];
    Hamburger.allStuffings = [Hamburger.STUFFING_CHEESE, Hamburger.STUFFING_POTATO, Hamburger.STUFFING_SALAD];

    Hamburger.prototype.addStuffing = function (stuffing) {
        this.stuffings.push(stuffing);
    };

    Hamburger.prototype.addTopping = function (topping) {
        this.toppings.push(topping);
    };

    Hamburger.prototype.removeTopping = function (topping) {
        var toppingIndex = this.toppings.indexOf(topping);
        this.toppings.splice(toppingIndex, 1);
    };

    Hamburger.prototype.removeStuffing = function (stuffing) {
        var stuffingIndex = this.toppings.indexOf(stuffing);
        this.stuffings.splice(stuffingIndex, 1);
    };

    Hamburger.prototype.getStuffings = function () {
        return this.stuffings.slice();
    };

    Hamburger.prototype.getToppings = function () {
        return this.toppings.slice();
    };

    /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */
    Hamburger.prototype.calculateCalories = function () {
        var supplementCalories = this.stuffings.reduce(countSupplementColories, 0) + this.toppings.reduce(countSupplementColories, 0);
        return this.size.calories + supplementCalories;
    };

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена гамбургера
     */
    Hamburger.prototype.calculatePrice = function () {
        var supplementPrice = this.stuffings.reduce(countSupplementPrice, 0) + this.toppings.reduce(countSupplementPrice, 0);
        return this.size.price + supplementPrice;
    };

    function countSupplementColories(calories, supplement) {
        return calories + supplement.calories;
    }

    function countSupplementPrice(price, supplement) {
        return price + supplement.price;
    }

    /**
     * Функция-конструктор для создания размера гамбургера
     * @param {String} name - описание размера
     * @param {Number} price - цена размера без добавок
     * @param {Number} calories - калории размера без добавок
     * @param {Number} maxStuffingsCount - максимальное количество начинок для данного размера
     */
    function Size(name, price, calories, maxStuffingsCount) {
        this.name = name;
        this.price = price;
        this.calories = calories;
        this.maxStuffingsCount = maxStuffingsCount;
    }

    // Функция-конструктор для начинок и топпингов
    function Supplement(name, price, calories) {
        this.name = name;
        this.price = price;
        this.calories = calories;
    }

    return Hamburger;
})();
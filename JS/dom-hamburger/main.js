document.addEventListener('DOMContentLoaded', start);

function start() {
    var sizeSelector = new SizeSelectionComponent('newHamburger');
    sizeSelector.onSizeSelect(sizeSelectHandler);
}

function sizeSelectHandler(size) {
    var hamburger = new Hamburger(size);
    createShop(hamburger);
}
function createShop(hamburger) {
    var bill = null;
    var toppings = new FillingComponent('toppings', Hamburger.allToppings);
    var stuffings = new FillingComponent('stuffings', Hamburger.allStuffings);
    var hamburgerInfoBlock = new HamburgerInfoComponent('hamburgerInfoBlock', hamburger);
    var countAndPrice = new CountAndPriceComponent();
    var buyButton = document.getElementById('buyButton');

    toppings.onItemAdd(addTopping);
    toppings.onItemRemove(removeTopping);
    stuffings.onItemAdd(addStuffing);
    stuffings.onItemRemove(removeStuffing);
    countAndPrice.setPrice(hamburger.calculatePrice());
    buyButton.addEventListener('click', issueBill);

    function addTopping(topping) {
        hamburger.addTopping(topping);
        hamburgerInfoBlock.updateToppings();
        countAndPrice.setPrice(hamburger.calculatePrice());
    }

    function removeTopping(topping) {
        hamburger.removeTopping(topping);
        hamburgerInfoBlock.updateToppings();
        countAndPrice.setPrice(hamburger.calculatePrice());
    }

    function addStuffing(stuffing) {
        hamburger.addStuffing(stuffing);
        hamburgerInfoBlock.updateStuffings();
        countAndPrice.setPrice(hamburger.calculatePrice());
    }

    function removeStuffing(stuffing) {
        hamburger.removeStuffing(stuffing);
        hamburgerInfoBlock.updateStuffings();
        countAndPrice.setPrice(hamburger.calculatePrice());
    }

    function issueBill() {
        if (bill === null) {
            bill = new BillComponent();
        }
        bill.toFormBill(hamburger, countAndPrice.getCount());
    }
}
function SizeSelectionComponent(selectionAreaId) {
    var sizeSelectListener = null;
    var selectionArea = document.getElementById(selectionAreaId);

    selectionArea.addEventListener('click', onSelectionAreaClickEvent);

    function onSelectionAreaClickEvent(event) {
        var target = event.target;
        if (target.tagName !== 'BUTTON') {
            return;
        }

        var sizeName = target.dataset.size;
        var size = Hamburger[sizeName] || Hamburger.SIZE_SMALL;
        openHamburgerFilling();
        if (sizeSelectListener) {
            sizeSelectListener(size);
        }
    }

    function openHamburgerFilling() {
        selectionArea.style.display = 'none';
        document.getElementById('hamburgerFilling').style.display = 'block';
    }

    this.onSizeSelect = function (listener) {
        sizeSelectListener = listener;
    };
}
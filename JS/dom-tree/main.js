/**
 * Функция для генерации структуры иерархии
 * в виде дерева каталогов и файлов.
 */
function generateTree() {
    var resources = document.getElementsByClassName('resource');
    var files = parseElements(resources);
    var tree = buildTree(files);
    var divTree = document.getElementById('tree');
    drawTree(tree, divTree);
}

function parseElements(elements) {
    var handledElements = [];

    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var fileInfo = element.querySelectorAll('ul > li');
        var obj = {
            name: element.querySelector('h2').textContent,
            type: fileInfo[0].lastChild.textContent,
            size: fileInfo[1].lastChild.textContent,
            location: fileInfo[2].lastChild.textContent,
            children: []
        };
        handledElements.push(obj);
    }
    return handledElements;
}

function buildTree(elements) {
    var rootElement = {
        name: 'ROOT',
        type: 'D',
        size: null,
        location: null,
        children: []
    };

    elementMap = {
        ROOT: rootElement
    };
    return elements.reduce(function (map, element) {
        if (!(element.location in map)) {
            map[element.location] = {
                children: []
            };
        }
        map[element.location].children.push(element);

        if (element.type === 'D') {
            var fakeElement = map[element.name];
            if (fakeElement) {
                element.children = fakeElement.children;
            }
            map[element.name] = element;
        }
        return map;
    }, elementMap).ROOT;
}

function drawTree(tree, drawingPlace) {
    var SIZE_UNITS = ['bytes', 'Kb', 'Mb', 'Gb'];

    drawingPlace.appendChild(insertDataAt(tree));

    function insertDataAt(data) {
        var node = document.createElement('li');
        var header;
        if (data.type === 'F') {
            header = createFileHeader(data.name, data.size);
        } else {
            header = createDirectoryHeader(data.name);
        }
        node.appendChild(createConnectorLine());
        node.appendChild(header);

        if (data.children.length !== 0) {
            var wrapper = createWrapper(node);
            data.children.forEach(function (child) {
                wrapper.appendChild(insertDataAt(child));
            });
        }
        return node;
    }

    function formatSize(size) {
        var index = 0;
        while (size >= 1024 && index < SIZE_UNITS.length - 1) {
            size /= 1024;
            index++;
        }
        return ' [' + size.toFixed(0) + ' ' + SIZE_UNITS[index] + ']';
    }

    function createConnectorLine() {
        var connectorLine = document.createElement('span');
        connectorLine.className = 'tree-connector-line';
        return connectorLine;
    }

    function createDirectoryHeader(name) {
        var directoryHeader = document.createElement('span');
        directoryHeader.className = 'header';
        directoryHeader.textContent = name;
        return directoryHeader;
    }

    function createFileHeader(name, size) {
        var fileHeader = document.createElement('span');
        fileHeader.className = 'file-header';
        fileHeader.textContent = name + formatSize(+size);
        return fileHeader;
    }

    function createWrapper(node) {
        var treeWrapper = node.appendChild(createTreeWrapper());
        treeWrapper.appendChild(createHierarchyLine());
        var ul = treeWrapper.appendChild(createUlTree());
        return ul;
    }

    function createTreeWrapper() {
        var treeWrapper = document.createElement('div');
        treeWrapper.className = 'tree-wrapper';
        return treeWrapper;
    }

    function createHierarchyLine() {
        var hierarchyLine = document.createElement('div');
        hierarchyLine.className = 'tree-hierarchy-line';
        return hierarchyLine;
    }

    function createUlTree() {
        var ulTree = document.createElement('ul');
        ulTree.className = 'tree';
        return ulTree;
    }
}
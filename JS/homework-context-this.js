/**
 * Необходимо без выполнения кода и открытия его в редакторе
 * определить результат выполнения.
 *
 * Чему будет равно значение переменной result?
 */
function mystery(input) {
    var secret = 4;
    input += 2;
    function mystery2(multiplier) {
        multiplier *= input;
        return secret * multiplier;
    }
    return mystery2;
}

function mystery3(param) {
    function mystery4(bonus) {
        return param(6) + bonus;
    }
    return mystery4;
}

var hidden = mystery(3);
var jumble = mystery3(hidden);
var result = jumble(2); //122

/**
 * Реализовать две функции repeat, sequence
 *
 * Напиши функцию создания генератора sequence(start, step).
 * Она при вызове возвращает другую функцию-генератор,
 * которая при каждом вызове дает число на 1 больше, и так до бесконечности.
 *
 * Начальное число, с которого начинать отсчет, и шаг, задается при создании генератора.
 * Шаг можно не указывать, тогда он будет равен одному. Начальное значение по умолчанию равно 0.
 * Генераторов можно создать сколько угодно.
 *
 * @param start - начальное число, с которого начинать отсчет.
*                 Начальное значение по умолчанию равно 0.
 * @param step -  шаг, задается при создании генератора.
 *                Шаг можно не указывать, тогда он будет равен одному
 *
 * @return function - вызове возвращает другую функцию-генератор,
 * которая при каждом вызове дает число начиная с start на step больше, и так до бесконечности 
 */
function sequence(start, step) {
    start = start || 0;
    step = step || 1;
    start -= step;
    return function () {
        start += step;
        return start;
    }
}

/**
 * Функция вызвает функцию func заданное число (times) раз
 *
 * @param func - функция, которая будет вызываться
 * @param times - сколько раз нужно вызвать функцию func
 */
function repeat(func, times) {
    while (times > 0) {
        func();
        --times;
    }
}

var generator = sequence(10, 3);
var generator2 = sequence(0, 2);

console.log(generator()); // 10
console.log(generator()); // 13

console.log(generator2()); // 0

repeat(generator2, 5); // [2, 4, 6, 8, 10]



/**
 * Реализовать конструктор для созадния гамбургером с описанными методами ниже.
 * Необходимо оформить с помощью шаблона "Модуль".
 *
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 */
var Hamburger = (function () {
    function Hamburger(size, stuffing) {

        var stuffings = [stuffing];
        var toppings = [];

        /**
         * Добавить начинку к гамбургеру. Можно добавить несколько
         * добавок, при условии, что они разные.
         *
         * Нельзя добавить начинку, если размер амбургера
         * Hamburger.SIZE_SMALL и кол-во начинку равно 5.
         *
         * Если размер гамбургера Hamburger.SIZE_LARGE,
         * можно добавлять не больше 10 начинку
         *
         * @param stuffing  Тип начинку
         */
        this.addStuffing = function (stuffing) {
            if (stuffings.length === size.maxStuffingsCount) {
                console.log('You have chosen a ' + size.caption + ', so you can choose a maximum of ' + size.maxStuffingsCount + ' fillings.');
                return;
            }
            var exists = stuffings.indexOf(stuffing) !== -1;
            if (exists) {
                console.log('You have already added this stuffing')
                return;
            }
            stuffings.push(stuffing);
        };

        /**
         * Добавить топпинг к гамбургеру. Можно добавить несколько,
         * при условии, что они разные.
         *
         * @param topping  Тип топпинга
         */
        this.addTopping = function (topping) {
            var exists = (toppings.indexOf(topping) !== -1);
            if (exists) {
                console.log('You already added this topping');
                return;
            }
            toppings.push(topping);
        };

        /**
         * Убрать топпинг, при условии, что он ранее был
         * добавлен.
         *
         * @param topping Тип топпинга
         */
        this.removeTopping = function (topping) {
            var toppingIndex = toppings.indexOf(topping);
            if (toppingIndex === -1) {
                console.log('You didn\'t add such a topping');
                return;
            }
            toppings.splice(toppingIndex, 1);
        };

        /**
         * Узнать размер гамбургера
         * @return {Number} размер гамбургера
         */
        this.getSize = function () {
            return size;
        };

        /**
         * Узнать начинку гамбургера
         * @return {Array} Массив добавленных начинок, содержит константы
         * Hamburger.STUFFING_*
         */
        this.getStuffing = function () {
            return stuffings.slice();
        };

        /**
         * Получить список добавок
         *
         * @return {Array} Массив добавленных добавок, содержит константы
         * Hamburger.TOPPING_*
         */
        this.getToppings = function () {
            return toppings.slice();
        };

        /**
         * Узнать калорийность
         * @return {Number} Калорийность в калориях
         */
        this.calculateCalories = function () {
            var supplementCalories = stuffings.reduce(countSupplementColories, 0) + toppings.reduce(countSupplementColories, 0);
            return size.calories + supplementCalories * size.supplementsCoefficient;
        };

        /**
         * Узнать цену гамбургера
         * @return {Number} Цена гамбургера
         */
        this.calculatePrice = function () {
            var supplementPrice = stuffings.reduce(countSupplementPrice, 0) + toppings.reduce(countSupplementPrice, 0);
            return size.price + supplementPrice * size.supplementsCoefficient;
        };
    }
    
    var countSupplementColories = function (calories, supplement) {
        return calories + supplement.calories;
    };
    var countSupplementPrice = function (price, supplement) {
        return price + supplement.price;
    };
    
    /**
     * Функция-конструктор для создания размера гамбургера
     * @param {String} caption - описание размера
     * @param {Number} price - цена размера без добавок
     * @param {Number} calories - калории размера без добавок
     * @param {Number} supplementsCoefficient - множитель добавок к гамбургеру. учитывается при расчете цены и калорий
     * @param {Number} maxStuffingsCount - максимальное количество начинок для данного размера
     */
    function Size(caption, price, calories, supplementsCoefficient, maxStuffingsCount) {
        this.caption = caption;
        this.price = price;
        this.calories = calories;
        this.supplementsCoefficient = supplementsCoefficient;
        this.maxStuffingsCount = maxStuffingsCount;
    }

    // Функция-конструктор для начинок и топпингов
    function Supplement(name, price, calories) {
        this.name = name;
        this.price = price;
        this.calories = calories;
    }

    /* Размеры, виды начинок и добавок
     * Можно добавить свои топпинги и начинки
     *
     * Размеры начинаются с SIZE_*
     * Начинки начинаются с STUFFING_*
     * Топпинги начинаются с TOPPING_*
     */

    Hamburger.SIZE_SMALL = new Size('small hamburger', 2.5, 400, 1, 5);
    Hamburger.SIZE_LARGE = new Size('big hamburger', 4, 800, 2, 10);
    Hamburger.STUFFING_CHEESE = new Supplement('Cheese', 1, 150);
    Hamburger.STUFFING_SALAD = new Supplement('Salad', 0.5, 5);
    Hamburger.STUFFING_POTATO = new Supplement('Potato', 0.75, 40);
    Hamburger.TOPPING_MAYO = new Supplement('Mayo', 0.5, 300);
    Hamburger.TOPPING_SPICE = new Supplement('Spice', 0.25, 2);

    return Hamburger;
})();
// Пример использования

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// добавим из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);

// добавим картофель
hamburger.addStuffing(Hamburger.STUFFING_POTATO);

// спросим сколько там калорий
console.log('Калории: ', hamburger.calculateCalories());

// сколько стоит
console.log('Цена: ', hamburger.calculatePrice());

// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);

// А сколько теперь стоит?
console.log('Цена с соусом ', hamburger.calculatePrice());

// большой ли гамбургер получился?
console.log('Большой ли гамбургер? ', hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false

// убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Сколько топпингов добавлено ', hamburger.getToppings().length); // 1


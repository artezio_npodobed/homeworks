/**
 * Воспользовавшись преимуществом подхода хранения методов в прототипе
 * с занятия "Наследование, Прототипирование",  обновить
 * функцию-конструктор "Габургер" из пердыдущего задания.
 *
 * Подход "Модуль", который требовалось сделать в прошлом задании,
 * должен тоже присутстовать.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 */
var Hamburger = (function () {
    function Hamburger(size, stuffing) {
        this.size = size;
        this.stuffings = [stuffing];
        this.toppings = [];
    }

    /**
     * Добавить начинку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     *
     * Нельзя добавить начинку, если размер амбургера
     * Hamburger.SIZE_SMALL и кол-во начинку равно 5.
     *
     * Если размер гамбургера Hamburger.SIZE_LARGE,
     * можно добавлять не больше 10 начинку
     *
     * @param stuffing  Тип начинку
     */
    Hamburger.prototype.addStuffing = function (stuffing) {
        if (this.stuffings.length === this.size.maxStuffingsCount) {
            console.log('You have chosen a ' + this.size.caption + ', so you can choose a maximum of ' + this.size.maxStuffingsCount + ' fillings.');
            return;
        }
        if (this.stuffings.indexOf(stuffing) !== -1) {
            console.log('You have already added this stuffing')
            return;
        }
        this.stuffings.push(stuffing);
    };

    /**
     * Добавить топпинг к гамбургеру. Можно добавить несколько,
     * при условии, что они разные.
     *
     * @param topping  Тип топпинга
     */
    Hamburger.prototype.addTopping = function (topping) {
        if (this.toppings.indexOf(topping) !== -1) {
            console.log('You have already added this topping');
            return;
        }
        this.toppings.push(topping);
    };

    /**
     * Убрать топпинг, при условии, что он ранее был
     * добавлен.
     *
     * @param topping Тип топпинга
     */
    Hamburger.prototype.removeTopping = function (topping) {
        var toppingIndex = this.toppings.indexOf(topping);
        if (toppingIndex === -1) {
            console.log('You didn\'t add such a topping');
            return;
        }
        this.toppings.splice(toppingIndex, 1);
    };

    /**
     * Узнать размер гамбургера
     * @return {SIZE_*} размер гамбургера
     */
    Hamburger.prototype.getSize = function () {
        return this.size;
    };

    /**
     * Узнать начинку гамбургера
     * @return {Array} Массив добавленных начинок, содержит константы
     * Hamburger.STUFFING_*
     */
    Hamburger.prototype.getStuffing = function () {
        return this.stuffings.slice();
    };

    /**
     * Получить список добавок
     *
     * @return {Array} Массив добавленных добавок, содержит константы
     * Hamburger.TOPPING_*
     */
    Hamburger.prototype.getToppings = function () {
        return this.toppings.slice();
    };

    /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */
    Hamburger.prototype.calculateCalories = function () {
        var supplementCalories = this.stuffings.reduce(countSupplementColories, 0) + this.toppings.reduce(countSupplementColories, 0);
        return this.size.calories + supplementCalories * this.size.supplementsCoefficient;
    };

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена гамбургера
     */
    Hamburger.prototype.calculatePrice = function () {
        var supplementPrice = this.stuffings.reduce(countSupplementPrice, 0) + this.toppings.reduce(countSupplementPrice, 0);
        return this.size.price + supplementPrice * this.size.supplementsCoefficient;
    };

    function countSupplementColories(calories, supplement) {
        return calories + supplement.calories;
    };
    function countSupplementPrice(price, supplement) {
        return price + supplement.price;
    };

    /**
     * Функция-конструктор для создания размера гамбургера
     * @param {String} caption - описание размера
     * @param {Number} price - цена размера без добавок
     * @param {Number} calories - калории размера без добавок
     * @param {Number} supplementsCoefficient - множитель добавок к гамбургеру. учитывается при расчете цены и калорий
     * @param {Number} maxStuffingsCount - максимальное количество начинок для данного размера
     */
    function Size(caption, price, calories, supplementsCoefficient, maxStuffingsCount) {
        this.caption = caption;
        this.price = price;
        this.calories = calories;
        this.supplementsCoefficient = supplementsCoefficient;
        this.maxStuffingsCount = maxStuffingsCount;
    }

    // Функция-конструктор для начинок и топпингов
    function Supplement(name, price, calories) {
        this.name = name;
        this.price = price;
        this.calories = calories;
    }

    /* Размеры, виды начинок и добавок
     * Можно добавить свои топпинги и начинки
     *
     * Размеры начинаются с SIZE_*
     * Начинки начинаются с STUFFING_*
     * Топпинги начинаются с TOPPING_*
     */

    Hamburger.SIZE_SMALL = new Size('small hamburger', 2.5, 400, 1, 5);
    Hamburger.SIZE_LARGE = new Size('big hamburger', 4, 800, 2, 10);
    Hamburger.STUFFING_CHEESE = new Supplement('Cheese', 1, 150);
    Hamburger.STUFFING_SALAD = new Supplement('Salad', 0.5, 5);
    Hamburger.STUFFING_POTATO = new Supplement('Potato', 0.75, 40);
    Hamburger.TOPPING_MAYO = new Supplement('Mayo', 0.5, 300);
    Hamburger.TOPPING_SPICE = new Supplement('Spice', 0.25, 2);

    return Hamburger;
})();

var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

// добавим из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);

// добавим картофель
hamburger.addStuffing(Hamburger.STUFFING_POTATO);

// спросим сколько там калорий
console.log('Калории: ', hamburger.calculateCalories());

// сколько стоит
console.log('Цена: ', hamburger.calculatePrice());

// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);

// А сколько теперь стоит?
console.log('Цена с соусом ', hamburger.calculatePrice());

// большой ли гамбургер получился?
console.log('Большой ли гамбургер? ', hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false

// убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);

console.log('Сколько топпингов добавлено ', hamburger.getToppings().length); // 1
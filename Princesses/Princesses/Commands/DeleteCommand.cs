﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class DeleteCommand : ICommand
    {
        PrincessRepository repository;

        public DeleteCommand(PrincessRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(IList<string> parameters)
        {
            if (parameters.Count != 1)
            {
                Console.WriteLine("\nIncorrect command.Enter command in following format:\ndelete NUMBER");
                return;
            }
            if (!int.TryParse(parameters[0], out int number))
            {
                Console.WriteLine("\nPrincess number must be a number");
                return;
            }

            try
            {
                string name = repository.Delete(number).Name;
                Console.WriteLine($"\nPrincess \"{name}\" has been removed.");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
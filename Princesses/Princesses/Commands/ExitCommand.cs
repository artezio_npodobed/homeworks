﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class ExitCommand : ICommand
    {
        public void Execute(IList<string> parameters)
        {
            if (parameters.Count != 0)
            {
                Console.WriteLine("\nIncorrect command. Enter command witout parameters");
                return;
            }
            Environment.Exit(0);
        }
    }
}

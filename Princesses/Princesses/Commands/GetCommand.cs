﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class GetCommand : ICommand
    {
        PrincessRepository repository;

        public GetCommand(PrincessRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(IList<string> parameters)
        {
            if (parameters.Count != 1)
            {
                Console.WriteLine("\nIncorrect command.Enter command in following format:\nget NUMBER");
                return;
            }
            if (!int.TryParse(parameters[0], out int number))
            {
                Console.WriteLine("\nPrincess number must be a number");
                return;
            }

            try
            {
                Princess princess = repository.Get(number);
                Console.WriteLine($"\n{princess.Name}\nAge: {princess.Age}\nHair: {princess.HairColor}\nEyes: {princess.EyeColor}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

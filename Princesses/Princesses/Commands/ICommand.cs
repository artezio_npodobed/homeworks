﻿using System.Collections.Generic;

namespace Princesses
{
    interface ICommand
    {
        void Execute(IList<string> parameters);
    }
}

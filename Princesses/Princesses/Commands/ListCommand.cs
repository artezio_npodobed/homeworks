﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class ListCommand : ICommand
    {
        PrincessRepository repository;

        public ListCommand(PrincessRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(IList<string> parameters)
        {
            if (parameters.Count != 0)
            {
                Console.WriteLine("\nIncorrect command. Enter command witout parameters");
                return;
            }

            IEnumerable<Princess> princesses = repository.GetAll();
            foreach (Princess princess in princesses)
            {
                Console.WriteLine($"\n{princess.Name}\nAge: {princess.Age}\nHair: {princess.HairColor}\nEyes: {princess.EyeColor}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class UpdateCommand : ICommand
    {
        PrincessRepository repository;

        public UpdateCommand(PrincessRepository repository)
        {
            this.repository = repository;
        }

        public void Execute(IList<string> parameters)
        {
            if (parameters.Count != 5)
            {
                Console.WriteLine("\nIncorrect command. Enter command in following format:\nupdate NUMBER NAME AGE HAIRCOLOR EYECOLOR");
                return;
            }
            if (!int.TryParse(parameters[0], out int number))
            {
                Console.WriteLine("\nPrincess number must be a number");
                return;
            }
            if (!int.TryParse(parameters[2], out int age))
            {
                Console.WriteLine("\nPrincess age must be a number");
                return;
            }

            try
            {
                repository.Update(new Princess(
                    number,
                    parameters[1],
                    age,
                    parameters[3],
                    parameters[4]));
                Console.WriteLine($"\nPrincess \"{parameters[1]}\" has been updated");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

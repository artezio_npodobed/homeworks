﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Princesses
{
    class Manager
    {
        Dictionary<string, ICommand> commands = new Dictionary<string, ICommand>();
        PrincessRepository repository = new PrincessRepository();
        IParser fileParser = new FileParser();
        IParser commandParser = new CommandParser();
        List<string> parameters = new List<string>();

        public Manager()
        {
            commands.Add("add", new AddCommand(repository));
            commands.Add("get", new GetCommand(repository));
            commands.Add("update", new UpdateCommand(repository));
            commands.Add("delete", new DeleteCommand(repository));
            commands.Add("list", new ListCommand(repository));
            commands.Add("exit", new ExitCommand());
        }

        public void FillRepository(string path)
        {
            int i = 1;
            string[] fileLines = File.ReadAllLines(path);
            foreach (var line in fileLines)
            {
                parameters = fileParser.Parse(line);
                if (!int.TryParse(parameters[0], out int number))
                {
                    throw new ArgumentException($"Princess number is not a number\nLine {i} in file");
                }
                if (!int.TryParse(parameters[2], out int age))
                {
                    throw new ArgumentException($"Princess age is not a number\nLine {i} in file");
                }
                repository.Add(new Princess(
                    number,
                    parameters[1],
                    age,
                    parameters[3],
                    parameters[4]));
                i++;
            }
        }

        public void ExecuteCommand(string commandLine)
        {
            string commandName = commandLine.Split(new char[] { ' ' })[0];
            string parametersString = commandLine.Substring(commandName.Length);
            parameters = commandParser.Parse(parametersString);

            if (!commands.TryGetValue(commandName, out ICommand command))
            {
                Console.WriteLine("Command does not exist");
                return;
            }
            if (parameters == null)
            {
                Console.WriteLine("Incorrect parameters. Try again");
                return;
            }
            command.Execute(parameters);
        }
    }
}

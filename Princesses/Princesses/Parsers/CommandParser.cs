﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class CommandParser : IParser
    {
        public List<string> Parse(string commandLine)
        {
            List<string> commandParameters = new List<string>();
            string[] paramsByQuotes = commandLine.Split(new char[] { '"' }, StringSplitOptions.None);
            for (int i = 0; i < paramsByQuotes.Length; i++)
            {
                if (i % 2 == 0)
                {
                    commandParameters.AddRange(paramsByQuotes[i].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries));
                }
                else
                {
                    commandParameters.Add(paramsByQuotes[i]);
                }
            }
            return commandParameters;
        }
    }
}

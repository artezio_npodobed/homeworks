﻿using System;
using System.Collections.Generic;

namespace Princesses
{
    class FileParser : IParser
    {
        public List<string> Parse(string fileString)
        {
            return new List<string>(fileString.Split(new string[] { " | " }, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}

﻿using System.Collections.Generic;

namespace Princesses
{
    interface IParser
    {
        List<string> Parse(string data);
    }
}

﻿using System;
using System.Linq;

namespace Princesses
{
    class Princess
    {
        string name, hairColor, eyeColor;
        int number, age;
        static string[] hairColors = new string[] { "Black", "Blonde", "Platinum-blonde", "Strawberry-blonde", "Red", "Brown" };
        static string[] eyeColors = new string[] { "Brown", "Blue", "Violet", "Hazel" };

        public int Number
        {
            get
            {
                return number;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("\nNumber must be positive", "Number");
                }
                number = value;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value.Length > 30)
                {
                    throw new ArgumentException("\nThe maximum length of the name is 30 characters", "Name");
                }
                name = value;
            }
        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value < 0 || value > 99)
                {
                    throw new ArgumentException("\nAge must be between 0 and 99", "Age");
                }
                age = value;
            }
        }
        public string HairColor
        {
            get
            {
                return hairColor;
            }
            set
            {
                if (!hairColors.Contains(value))
                    throw new ArgumentException("\nHair color can only be one of these:\n" + string.Join(", ", hairColors), "hairColor");
                hairColor = value;
            }
        }
        public string EyeColor
        {
            get
            {
                return eyeColor;
            }
            set
            {
                if (!eyeColors.Contains(value))
                {
                    throw new ArgumentException("\nEye color can only be one of these:\n" + string.Join(", ", eyeColors), "eyeColor");
                }
                eyeColor = value;
            }
        }

        public Princess(int number, string name, int age, string hairColor, string eyeColor)
        {
            Number = number;
            Name = name;
            Age = age;
            HairColor = hairColor;
            EyeColor = eyeColor;
        }
    }
}

﻿using System;

namespace Princesses
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager();

            try
            {
                manager.FillRepository(@"..\..\disney-princesses.txt");
                Console.WriteLine("Application started successfully. Enter command:");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Application started with an error. Check the initial data");
                Console.WriteLine(ex.Message);
                Console.ReadKey();
                return;
            }

            while (true)
            {
                manager.ExecuteCommand(Console.ReadLine());
            }
        }
    }
}

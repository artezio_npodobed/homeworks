﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Princesses
{
    class PrincessRepository
    {
        List<Princess> princesses;

        public PrincessRepository()
        {
            princesses = new List<Princess>();
        }

        public void Add(Princess princess)
        {
            if (princesses.Any(x => x.Number == princess.Number))
            {
                throw new ArgumentException($"\nPrincess with number {princess.Number} is exist");
            }
            princesses.Add(princess);
        }

        public Princess Delete(int number)
        {
            Princess princess = princesses.FirstOrDefault(x => x.Number == number);
            if (princess == null)
            {
                throw new ArgumentException($"\nPrincess with number {number} is not exist");
            }
            princesses.Remove(princess);
            return princess;
        }

        public Princess Get(int number)
        {
            Princess princess = princesses.FirstOrDefault(x => x.Number == number);
            if (princess == null)
            {
                throw new ArgumentException($"\nPrincess with number {number} is not exist");
            }
            return princess;
        }

        public IEnumerable<Princess> GetAll()
        {
            return princesses.OrderBy(x => x.Number);
        }

        public void Update(Princess princess)
        {
            if (princesses.RemoveAll(x => x.Number == princess.Number) == 0)
            {
                throw new ArgumentException($"\nPrincess with number {princess.Number} is not exist");
            }
            princesses.Add(princess);
        }
    }
}

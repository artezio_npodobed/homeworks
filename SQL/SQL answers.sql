-- 1) Есть ли дополнительная возможность нормализовать таблицы в приведенной схеме?
--    Можно ввести дополннительную таблицу городов и заменить значения столбца CITY в таблицах SELLERS и CUSTOMERS внешним ключом. (3-я НФ)

-- 2) Вывести информацию о заказах, у которых нет покупателей или продавцов
SELECT *
    FROM ORDERS
    WHERE CNUM IS NULL OR SNUM IS NULL;

-- 3) Вывести информацию о заказах, у которых у покупателей нет предпочтительных продавцов
SELECT O.*
    FROM ORDERS O
        INNER JOIN CUSTOMERS C ON O.CNUM = C.CNUM
    WHERE C.SNUM IS NULL;

-- 4) Вывести информацию о заказах, именах покупателей и продавцов, если продавца или покупателя нет, вывести null
SELECT O.*, C.CNAME, S.SNAME
    FROM ORDERS O
        LEFT JOIN CUSTOMERS C ON O.CNUM = C.CNUM
        LEFT JOIN SELLERS S ON O.SNUM = S.SNUM;

-- 5) Вывести полную информацию о продавцах и заказах, если продавец не не имеет заказов, вывести null
SELECT *
    FROM SELLERS S
        LEFT JOIN ORDERS O ON S.SNUM = O.SNUM;

-- 6) Вывести уникальные пары имен покупателей и продавцов по таблице заказов. Имена должны быть определены
SELECT DISTINCT S.SNAME, C.CNAME
    FROM ORDERS O
        INNER JOIN SELLERS S ON O.SNUM = S.SNUM
        INNER JOIN CUSTOMERS C ON C.CNUM = O.CNUM;

-- 7) Вывести уникальные пары покупателей имеющих заказы в один и тот же день. 
SELECT DISTINCT C1.CNAME, C2.CNAME
    FROM ORDERS O1
        INNER JOIN ORDERS O2 ON O1.ODATE = O2.ODATE
        INNER JOIN CUSTOMERS C1 ON C1.CNUM = O1.CNUM
        INNER JOIN CUSTOMERS C2 ON C2.CNUM = O2.CNUM
	WHERE O1.CNUM < O2.CNUM;

-- 8) Вывести строки с минимальными и максимальными суммами заказов и именем покупателя, столбцом указывающем самая высокая или низкая сумма
SELECT C.CNAME, 'MAX' TYPE, MAX(O.AMT) VALUE
    FROM CUSTOMERS C
        INNER JOIN ORDERS O ON C.CNUM = O.CNUM
    GROUP BY C.CNAME
UNION
SELECT C.CNAME, 'MIN', MIN(O.AMT)
    FROM CUSTOMERS C
        INNER JOIN ORDERS O ON C.CNUM = O.CNUM
    GROUP BY C.CNAME;

-- 9) Вывести полную информацию о покупателях у котохых есть заказы с продавцом помимо предпочтительного
SELECT DISTINCT C.*
    FROM CUSTOMERS C
        INNER JOIN ORDERS O ON C.CNUM = O.CNUM
    WHERE C.SNUM <> O.SNUM;

-- 10) Вывести полную информацию о заказах покупатели и продавцы которых живут в разных городах
SELECT O.*
    FROM ORDERS O
        INNER JOIN CUSTOMERS C ON C.CNUM = O.CNUM
        INNER JOIN SELLERS S ON S.SNUM = O.SNUM
    WHERE S.CITY <> C.CITY;

-- 11) Вывести дату и среднюю сумму заказа на эту дату.
SELECT O.ODATE, AVG ( O.AMT ) AverageAMT
    FROM ORDERS O
    GROUP BY O.ODATE;

-- 12) Вывести имена всех продавцов которые имеют ведут более двух заказов
SELECT S.SNAME
    FROM SELLERS S
        INNER JOIN ORDERS O ON O.SNUM = S.SNUM
    GROUP BY S.SNAME
    HAVING COUNT ( S.SNAME ) > 2;

-- 13) Вывести все заказы сумма которых больше средней суммы всех заказов
SELECT *
    FROM ORDERS
    WHERE AMT > ( SELECT AVG ( AMT ) FROM ORDERS);

-- 14) Вывести все заказы покупателей с рейтингом выше среднего
SELECT O.*
    FROM ORDERS O
        INNER JOIN CUSTOMERS C ON C.CNUM = O.CNUM
    WHERE C.RATING > (SELECT AVG ( RATING ) FROM CUSTOMERS);

-- 15) Вставить в таблицу TMPDATA данные всех заказов и имена заказчиков из 'London' и 'Boston'
INSERT INTO TMPDATA
        SELECT O.CNUM, O.SNUM, C.CNAME, C.CITY, O.ODATE, O.AMT
            FROM ORDERS O
	            INNER JOIN CUSTOMERS C ON C.CNUM = O.CNUM
            WHERE C.CITY = 'London' OR C.CITY = 'Boston';

-- 16) Изменить в таблице TMPDATA дату всех заказов заказчика с рейтингом 200 на 01/01/1990
UPDATE TMPDATA
    SET ODATE = '01/01/1990'
	WHERE CNUM IN (SELECT CNUM FROM CUSTOMERS WHERE RATING = 200);

-- 17) Удалить из таблицы TMPDATA все заказы продавца 'Peel'
DELETE FROM TMPDATA
	WHERE SNUM IN (SELECT SNUM FROM SELLERS WHERE SNAME = 'Peel');

-- 18) Вставить в таблицу TMPDATA все заказы продавцов которые имеют комисионные ниже чем средние комисионные продавцов имеющих заказы
INSERT INTO TMPDATA
        SELECT O.CNUM, O.SNUM, C.CNAME, C.CITY, O.ODATE, O.AMT
            FROM ORDERS O
	            LEFT JOIN CUSTOMERS C ON C.CNUM = O.CNUM
				INNER JOIN SELLERS S ON S.SNUM = O.SNUM
            WHERE S.COMM < ( SELECT AVG ( COMM ) 
                                 FROM SELLERS S
	                                 INNER JOIN ( SELECT DISTINCT SNUM FROM ORDERS ) O ON O.SNUM = S.SNUM);
